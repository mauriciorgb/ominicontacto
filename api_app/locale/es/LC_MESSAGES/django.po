# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-05 16:30-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: api_app/apps.py:289
msgid "Lista de agentes activos de un Grupo"
msgstr ""

#: api_app/apps.py:291
msgid "Campañas relacionadas a un supervisor"
msgstr ""

#: api_app/apps.py:293
msgid "Opciones de calificacion de una campaña (id campaña externo)"
msgstr ""

#: api_app/apps.py:296
msgid "Opciones de calificacion de una campaña (id campaña oml)"
msgstr ""

#: api_app/apps.py:299
msgid "Ver/editar calificación cliente"
msgstr ""

#: api_app/apps.py:301
msgid "Crear calificacion cliente"
msgstr ""

#: api_app/apps.py:303
msgid "Crear contacto"
msgstr ""

#: api_app/apps.py:305
msgid "Metadata de la base de datos de una campaña"
msgstr ""

#: api_app/apps.py:307
msgid "Crear nuevo rol"
msgstr ""

#: api_app/apps.py:309
msgid "Eliminar rol"
msgstr ""

#: api_app/apps.py:311
msgid "Actualizar permisos de Rol"
msgstr ""

#: api_app/apps.py:313
msgid "Devuelve información de los agentes en el sistema"
msgstr ""

#: api_app/apps.py:316
msgid "Reporte de llamadas entrantes de supervisión."
msgstr ""

#: api_app/apps.py:318
msgid "Reporte de llamadas salientes no dialer de supervisión."
msgstr ""

#: api_app/apps.py:322
msgid "Ejecuta acciones de supervisión sobre agente (Deslogueo, pausas, etc..)"
msgstr ""

#: api_app/apps.py:325
msgid "Cantidades por tipos de llamada en una campaña"
msgstr ""

#: api_app/apps.py:328
msgid "Cantidades por tipo de calificación en una campaña"
msgstr ""

#: api_app/apps.py:331
msgid "Reasignar una Agenda a otro Agente"
msgstr ""

#: api_app/apps.py:333
msgid "Información de contacto de una  Agenda"
msgstr ""

#: api_app/apps.py:335
msgid "Exportar reporte de contactados de una campaña a csv"
msgstr ""

#: api_app/apps.py:338
msgid "Exportar reporte de calificados de una campaña a csv"
msgstr ""

#: api_app/apps.py:341
msgid "Exportar reporte de no atendidos de una campaña a csv"
msgstr ""

#: api_app/apps.py:344 api_app/apps.py:347
msgid "Exportar reporte de calificaciones de una campaña a csv"
msgstr ""

#: api_app/apps.py:350
msgid "API para exportar resultados de base contactados a csv"
msgstr ""

#: api_app/apps.py:353
msgid "Contactos de una campaña"
msgstr ""

#: api_app/apps.py:355
msgid "Ejecuta un click 2 call a una campaña"
msgstr ""

#: api_app/apps.py:357
msgid "Ejecuta un click 2 call fuera de campaña"
msgstr ""

#: api_app/apps.py:359
msgid "Ejecuta el login del agente en Asterisk."
msgstr ""

#: api_app/apps.py:361
msgid "Ejecuta el logout del agente en Asterisk"
msgstr ""

#: api_app/apps.py:363
msgid "Logout del agente en OML"
msgstr ""

#: api_app/apps.py:365
msgid "Pone al agente en una pausa"
msgstr ""

#: api_app/apps.py:367
msgid "Saca al agente de una pausa"
msgstr ""

#: api_app/apps.py:369
msgid "Establece el estado Ringing del Agente"
msgstr ""

#: api_app/apps.py:371
msgid "Establece el estado Reject del Agente"
msgstr ""

#: api_app/apps.py:373
msgid "Devuelve credenciales SIP de un agente"
msgstr ""

#: api_app/apps.py:375
msgid "Establece el estado de revisión de una Auditoría"
msgstr ""

#: api_app/apps.py:378
msgid "Almacena en la base de datos los contactos subidos en archivo csv"
msgstr ""

#: api_app/apps.py:381
msgid "Detecta si una llamada esta calificada."
msgstr ""

#: api_app/apps.py:384
msgid "Reenvía la llave de la instancia registrada por email"
msgstr ""

#: api_app/apps.py:386
msgid "Retorna el archivo de grabación especificado"
msgstr ""

#: api_app/apps.py:389
msgid "Retorna Zip con archivos de grabación especificados"
msgstr ""

#: api_app/apps.py:392
msgid "Retorna URL del archivos de grabación asociado al callid"
msgstr ""

#: api_app/apps.py:395
msgid "Devuelve los contactos asignados de una campaña preview"
msgstr ""

#: api_app/apps.py:398
msgid "Loggea el evento hold o unhold"
msgstr ""

#: api_app/apps.py:401
msgid "Lista audios de asterisk"
msgstr ""

#: api_app/apps.py:404
msgid "Lista de grupos de agentes"
msgstr ""

#: api_app/apps.py:407
msgid "Lista de Agentes"
msgstr ""

#: api_app/apps.py:410
msgid "Dashboard de supervision"
msgstr ""

#: api_app/apps.py:413
msgid "Auditoría a supervisores"
msgstr ""

#: api_app/apps.py:416
msgid "Listas de nombres de columnas para Base de datos de Contactos"
msgstr ""

#: api_app/apps.py:419
msgid "Reinicia servicio Wombat Dialer"
msgstr ""

#: api_app/apps.py:422
msgid "Estado del servicio Wombat Dialer"
msgstr ""

#: api_app/apps.py:425
msgid "Lista de agentes por campaña"
msgstr ""

#: api_app/apps.py:428
msgid "Actualiza los agentes de una campaña"
msgstr ""

#: api_app/apps.py:431
msgid "Obtiene todos los agentes activos"
msgstr ""

#: api_app/apps.py:434
msgid "Obtiene las pausas que no estan eliminadas"
msgstr ""

#: api_app/apps.py:437
msgid "Lista los conjuntos de pausas"
msgstr ""

#: api_app/apps.py:440
msgid "Obtiene el detalle de un conjunto de pausas"
msgstr ""

#: api_app/apps.py:443
msgid "Crea un conjunto de pausas"
msgstr ""

#: api_app/apps.py:446
msgid "Actualiza un conjunto de pausas"
msgstr ""

#: api_app/apps.py:449
msgid "Elimina un conjunto de pausas"
msgstr ""

#: api_app/apps.py:452
msgid "Crea una configuracion de pausa"
msgstr ""

#: api_app/apps.py:455
msgid "Actualiza una configuracion de pausa"
msgstr ""

#: api_app/apps.py:458
msgid "Elimina una configuracion de pausa"
msgstr ""

#: api_app/apps.py:461
msgid "Lista los sitios externos"
msgstr ""

#: api_app/apps.py:464
msgid "Obtiene el detalle de un sitio externo"
msgstr ""

#: api_app/apps.py:467
msgid "Crea un sitio externo"
msgstr ""

#: api_app/apps.py:470
msgid "Actualiza un sitio externo"
msgstr ""

#: api_app/apps.py:473
msgid "Elimina un sitio externo"
msgstr ""

#: api_app/apps.py:476
msgid "Lista las autenticaciones para sitios externos"
msgstr ""

#: api_app/apps.py:479
msgid "Obtiene el detalle de una autenticacion para sitio externo"
msgstr ""

#: api_app/apps.py:482
msgid "Crea una autenticacion para sitio externo"
msgstr ""

#: api_app/apps.py:485
msgid "Actualiza una autenticacion para sitio externo"
msgstr ""

#: api_app/apps.py:488
msgid "Elimina una autenticacion para sitio externo"
msgstr ""

#: api_app/apps.py:491
msgid "Oculta un sitio externo"
msgstr ""

#: api_app/apps.py:494
msgid "Desoculta un sitio externo"
msgstr ""

#: api_app/apps.py:497
msgid "Lista las calificaciones"
msgstr ""

#: api_app/apps.py:500
msgid "Crea una calificacion"
msgstr ""

#: api_app/apps.py:503
msgid "Actualiza una calificacion"
msgstr ""

#: api_app/apps.py:506
msgid "Elimina una calificacion"
msgstr ""

#: api_app/apps.py:509
msgid "Obtiene detalle de una calificacion"
msgstr ""

#: api_app/apps.py:512
msgid "Lista los destinos entrantes"
msgstr ""

#: api_app/apps.py:515
msgid "Lista tipos de destinos entrantes"
msgstr ""

#: api_app/apps.py:518
msgid "Lista los sistemas externos"
msgstr ""

#: api_app/apps.py:521
msgid "Crea un sistema externo"
msgstr ""

#: api_app/apps.py:524
msgid "Actualiza un sistema externo"
msgstr ""

#: api_app/apps.py:527
msgid "Obtiene detalle de un sistema externo"
msgstr ""

#: api_app/apps.py:530
msgid "Obtiene los agentes para asignar a un sistema externo"
msgstr ""

#: api_app/apps.py:533
msgid "Lista los formularios"
msgstr ""

#: api_app/apps.py:536
msgid "Crea un formulario"
msgstr ""

#: api_app/apps.py:539
msgid "Actualiza un formulario"
msgstr ""

#: api_app/apps.py:542
msgid "Obtiene detalle de un formulario"
msgstr ""

#: api_app/apps.py:545
msgid "Elimina un formulario"
msgstr ""

#: api_app/apps.py:548
msgid "Oculta un formulario"
msgstr ""

#: api_app/apps.py:551
msgid "Desoculta un formulario"
msgstr ""

#: api_app/apps.py:554
msgid "Lista las pausas"
msgstr ""

#: api_app/apps.py:557
msgid "Crea una pausa"
msgstr ""

#: api_app/apps.py:560
msgid "Actualiza una pausa"
msgstr ""

#: api_app/apps.py:563
msgid "Obtiene detalle de una pausa"
msgstr ""

#: api_app/apps.py:566
msgid "Elimina una pausa"
msgstr ""

#: api_app/apps.py:569
msgid "Reactiva una pausa"
msgstr ""

#: api_app/apps.py:572
msgid "Lista las rutas entrantes"
msgstr ""

#: api_app/apps.py:575
msgid "Crea una ruta entrante"
msgstr ""

#: api_app/apps.py:578
msgid "Actualiza una ruta entrante"
msgstr ""

#: api_app/apps.py:581
msgid "Obtiene detalle de una ruta entrante"
msgstr ""

#: api_app/apps.py:584
msgid "Elimina una ruta entrante"
msgstr ""

#: api_app/apps.py:587
msgid "Obtiene los destinos disponibles"
msgstr ""

#: api_app/apps.py:590
msgid "Lista las rutas salientes"
msgstr ""

#: api_app/apps.py:593
msgid "Crea una ruta saliente"
msgstr ""

#: api_app/apps.py:596
msgid "Actualiza una ruta saliente"
msgstr ""

#: api_app/apps.py:599
msgid "Obtiene detalle de una ruta saliente"
msgstr ""

#: api_app/apps.py:602
msgid "Elimina una ruta saliente"
msgstr ""

#: api_app/apps.py:605
msgid "Obtiene las troncales sip para rutas salientes"
msgstr ""

#: api_app/apps.py:608
msgid "Obtiene las troncales huerfanas de una ruta saliente"
msgstr ""

#: api_app/apps.py:611
msgid "Reordena las rutas salientes"
msgstr ""

#: api_app/apps.py:614
msgid "Lista los grupos horarios"
msgstr ""

#: api_app/apps.py:617
msgid "Crea un grupo horario"
msgstr ""

#: api_app/apps.py:620
msgid "Actualiza un grupo horario"
msgstr ""

#: api_app/apps.py:623
msgid "Obtiene detalle de un grupo horario"
msgstr ""

#: api_app/apps.py:626
msgid "Elimina un grupo horario"
msgstr ""

#: api_app/apps.py:629
msgid "Lista los ivrs"
msgstr ""

#: api_app/apps.py:632
msgid "Crea un ivr"
msgstr ""

#: api_app/apps.py:635
msgid "Actualiza un ivr"
msgstr ""

#: api_app/apps.py:638
msgid "Obtiene detalle de un ivr"
msgstr ""

#: api_app/apps.py:641
msgid "Elimina un ivr"
msgstr ""

#: api_app/apps.py:644
msgid "Obtiene los audios disponibles para ivr"
msgstr ""

#: api_app/apps.py:647
msgid "Obtiene los destinos por tipo para un ivr"
msgstr ""

#: api_app/forms.py:52 api_app/forms.py:97
msgid "El agente no participa en la campaña."
msgstr ""

#: api_app/forms.py:61
msgid "El contacto no corresponde a la campaña."
msgstr ""

#: api_app/forms.py:99
msgid "El agente no corresponde al sistema externo."
msgstr ""

#: api_app/services/base_datos_contacto_service.py:44
msgid "Ya existe una base de datos de contactos con ese nombre"
msgstr ""

#: api_app/services/base_datos_contacto_service.py:48
msgid ""
"El archivo especificado para realizar la importación de contactos no es "
"válido."
msgstr ""

#: api_app/services/base_datos_contacto_service.py:53
#: api_app/views/administrador.py:194
msgid "El archivo a procesar tiene nombres de columnas repetidos."
msgstr ""

#: api_app/services/base_datos_contacto_service.py:127
msgid "Archivo excede máximo de filas permitidas"
msgstr ""

#: api_app/services/base_datos_contacto_service.py:131
msgid "Error al parsear el archivo csv"
msgstr ""

#: api_app/views/administrador.py:77
msgid "Se esperaba el campo \"name\""
msgstr ""

#: api_app/views/administrador.py:82
msgid "Ya existe un rol con ese nombre"
msgstr ""

#: api_app/views/administrador.py:107
msgid "Se esperaba el campo \"role_id\" (numérico)"
msgstr ""

#: api_app/views/administrador.py:114 api_app/views/administrador.py:144
msgid "Id de Rol incorrecto"
msgstr ""

#: api_app/views/administrador.py:118
msgid "Se esperaba el campo \"permissions\" (lista)"
msgstr ""

#: api_app/views/administrador.py:123
msgid "Lista de permisos incorrecta"
msgstr ""

#: api_app/views/administrador.py:149
msgid "No se puede borrar un rol asignado a usuarios."
msgstr ""

#: api_app/views/administrador.py:184
#, python-brace-format
msgid "La longitud del nombre no debe exceder los {0} caracteres"
msgstr ""

#: api_app/views/administrador.py:188
msgid "la extensión del archivo no es .CSV"
msgstr ""

#: api_app/views/administrador.py:191
msgid "falta parámetro filename en request"
msgstr ""

#: api_app/views/administrador.py:201
msgid "no se pudo crear la base de datos de contacto"
msgstr ""

#: api_app/views/administrador.py:213
msgid "falta parámetro:"
msgstr ""

#: api_app/views/administrador.py:225
msgid "lista de campos teléfono vacia"
msgstr ""

#: api_app/views/administrador.py:228
msgid "campo de teléfono no coincide con nombre de columna"
msgstr ""

#: api_app/views/administrador.py:236
msgid "campo de id externo no coincide con nombre de columna"
msgstr ""

#: api_app/views/administrador.py:280
#, python-brace-format
msgid "Error en el intento de conexion a: {0} debido {1}"
msgstr ""

#: api_app/views/administrador.py:285
msgid "Error en el servidor externo"
msgstr ""

#: api_app/views/agente.py:68
msgid "Error al generar sip password"
msgstr ""

#: api_app/views/agente.py:220 api_app/views/agente.py:255
#: api_app/views/base.py:75 api_app/views/base.py:207
msgid "Hubo errores en los datos recibidos"
msgstr ""

#: api_app/views/agente.py:221 api_app/views/base.py:86
#: api_app/views/base.py:218
msgid "Sistema externo inexistente."
msgstr ""

#: api_app/views/agente.py:246
msgid "Error al ejecutar la llamada"
msgstr ""

#: api_app/views/agente.py:276
msgid "Usted no tiene permiso para realizar esta acción"
msgstr ""

#: api_app/views/agente.py:289
msgid "Valor de destination inválido"
msgstr ""

#: api_app/views/agente.py:299
msgid "Valor de destination_type inválido"
msgstr ""

#: api_app/views/agente.py:303
msgid "Llamada fuera de campaña iniciada"
msgstr ""

#: api_app/views/agente.py:483
msgid "Auditoría inexistente"
msgstr ""

#: api_app/views/agente.py:488
msgid "No tiene permiso para modificar la auditoría"
msgstr ""

#: api_app/views/base.py:98 api_app/views/base.py:230
msgid "Debe indicar un idCampaign válido."
msgstr ""

#: api_app/views/base.py:110 api_app/views/base.py:243
msgid "Campaña inexistente."
msgstr ""

#: api_app/views/base.py:117 api_app/views/base.py:250
msgid "No tiene permiso para editar la campaña."
msgstr ""

#: api_app/views/base.py:126
msgid "Se recibieron campos incorrectos"
msgstr ""

#: api_app/views/base.py:134
msgid "El campo es obligatorio"
msgstr ""

#: api_app/views/base.py:175
msgid "Contacto agregado"
msgstr ""

#: api_app/views/call_disposition.py:43
msgid "Se obtuvieron las calificaciones de forma exitosa"
msgstr ""

#: api_app/views/call_disposition.py:54
msgid "Error al obtener las calificaciones"
msgstr ""

#: api_app/views/call_disposition.py:69
msgid "Se obtuvo el detalle de la calificacion de forma exitosa"
msgstr ""

#: api_app/views/call_disposition.py:79 api_app/views/call_disposition.py:208
msgid "No existe la calificacion"
msgstr ""

#: api_app/views/call_disposition.py:84
msgid "Error al obtener el detalle de la calificacion"
msgstr ""

#: api_app/views/call_disposition.py:112
msgid "Se creo la calificacion de forma exitosa"
msgstr ""

#: api_app/views/call_disposition.py:119 api_app/views/call_disposition.py:162
msgid "Error al hacer la peticion"
msgstr ""

#: api_app/views/call_disposition.py:126
msgid "Error al crear la calificacion"
msgstr ""

#: api_app/views/call_disposition.py:154
msgid "Se actualizo la calificacion de forma exitosa"
msgstr ""

#: api_app/views/call_disposition.py:170
msgid "No existe la calificacion que se quiere actualizar"
msgstr ""

#: api_app/views/call_disposition.py:176
msgid "Error al actualizar la calificacion"
msgstr ""

#: api_app/views/call_disposition.py:192
msgid "Se elimino la calificacion de forma exitosa"
msgstr ""

#: api_app/views/call_disposition.py:200
msgid "No está permitido eliminar una calificacion reservada del sistema"
msgstr ""

#: api_app/views/call_disposition.py:213
msgid "Error al eliminar la calificacion"
msgstr ""

#: api_app/views/campaigns/add_agents_to_campaign.py:50
#: api_app/views/campaigns/add_agents_to_campaign.py:201
#: api_app/views/external_system.py:173
msgid "Se obtuvieron los agentes de forma exitosa"
msgstr ""

#: api_app/views/campaigns/add_agents_to_campaign.py:62
#: api_app/views/supervisor.py:407 api_app/views/supervisor.py:422
msgid "No existe la campaña"
msgstr ""

#: api_app/views/campaigns/add_agents_to_campaign.py:82
#, python-brace-format
msgid "QueueAdd failed - agente: {0} de la campana: {1} "
msgstr ""

#: api_app/views/campaigns/add_agents_to_campaign.py:111
#, python-brace-format
msgid "QueueRemove failed - agente: {0} de la campana: {1}"
msgstr ""

#: api_app/views/campaigns/add_agents_to_campaign.py:122
msgid "No existe el agente en cola"
msgstr ""

#: api_app/views/campaigns/add_agents_to_campaign.py:137
msgid "Se agregaron los agentes de forma exitosa a la campaña"
msgstr ""

#: api_app/views/campaigns/add_agents_to_campaign.py:144
msgid "No existe la campaña, no se pueden agregar agentes"
msgstr ""

#: api_app/views/campaigns/add_agents_to_campaign.py:160
msgid ""
"No existe el agente,                                        no se puede "
"eliminar de la member queue"
msgstr ""

#: api_app/views/campaigns/add_agents_to_campaign.py:172
msgid ""
"No existe el agente,                                        no se puede "
"crear la member queue"
msgstr ""

#: api_app/views/campaigns/add_agents_to_campaign.py:188
msgid "No se pudo confirmar la creación del dialplan"
msgstr ""

#: api_app/views/campaigns/add_agents_to_campaign.py:220
msgid "Error al obtener los agentes activos"
msgstr ""

#: api_app/views/external_site.py:43
msgid "Se obtuvieron los sitios externos de forma exitosa"
msgstr ""

#: api_app/views/external_site.py:53
msgid "Error al obtener los sitios externos"
msgstr ""

#: api_app/views/external_site.py:70
msgid "Se creo el sitio externo de forma exitosa"
msgstr ""

#: api_app/views/external_site.py:84
msgid "Error al crear el sitio externo"
msgstr ""

#: api_app/views/external_site.py:101
msgid "Se actualizo el sitio externo de forma exitosa"
msgstr ""

#: api_app/views/external_site.py:118
msgid "No existe el sitio externo que se quiere actualizar"
msgstr ""

#: api_app/views/external_site.py:124
msgid "Error al actualizar el sitio externo"
msgstr ""

#: api_app/views/external_site.py:140
msgid "Se obtuvo la informacion del sitio externo de forma exitosa"
msgstr ""

#: api_app/views/external_site.py:149
msgid "Error al obtener el detalle del sitio externo"
msgstr ""

#: api_app/views/external_site.py:165
msgid "Se elimino el sitio externo de forma exitosa"
msgstr ""

#: api_app/views/external_site.py:171
msgid "No está permitido eliminar un sitio externo asociado a una campaña"
msgstr ""

#: api_app/views/external_site.py:179
msgid "No existe el sitio externo"
msgstr ""

#: api_app/views/external_site.py:184
msgid "Error al eliminar el sitio externo"
msgstr ""

#: api_app/views/external_site.py:199
msgid "Se oculto el sitio externo de forma exitosa"
msgstr ""

#: api_app/views/external_site.py:207
msgid "Error al ocultar el sitio externo"
msgstr ""

#: api_app/views/external_site.py:222
msgid "Se desoculto el sitio externo de forma exitosa"
msgstr ""

#: api_app/views/external_site.py:230
msgid "Error al desocultar el sitio externo"
msgstr ""

#: api_app/views/external_site_authentication.py:42
msgid "Se obtuvieron las autenticaciones para sitios externos de forma exitosa"
msgstr ""

#: api_app/views/external_site_authentication.py:52
msgid "Error al obtener las autenticaciones para sitios externos"
msgstr ""

#: api_app/views/external_site_authentication.py:69
msgid "Se creo la autenticacion para sitio externo de forma exitosa"
msgstr ""

#: api_app/views/external_site_authentication.py:84
msgid "Error al crear la autenticacion para sitio externo"
msgstr ""

#: api_app/views/external_site_authentication.py:101
msgid "Se actualizo la autenticacion para sitio externo de forma exitosa"
msgstr ""

#: api_app/views/external_site_authentication.py:119
msgid "No existe la autenticacion para sitio externo que se quiere actualizar"
msgstr ""

#: api_app/views/external_site_authentication.py:125
msgid "Error al actualizar la autenticacion para sitio externo"
msgstr ""

#: api_app/views/external_site_authentication.py:141
msgid ""
"Se obtuvo la informacion de la autenticacion para sitio externo de forma "
"exitosa"
msgstr ""

#: api_app/views/external_site_authentication.py:151
msgid "Error al obtener el detalle de la autenticacion para sitio externo"
msgstr ""

#: api_app/views/external_site_authentication.py:167
msgid "Se elimino la autenticacion para sitio externo de forma exitosa"
msgstr ""

#: api_app/views/external_site_authentication.py:173
msgid "No está permitido eliminar una autenticacion que tiene sitios externos"
msgstr ""

#: api_app/views/external_site_authentication.py:181
msgid "No existe la autenticacion para sitio externo"
msgstr ""

#: api_app/views/external_site_authentication.py:186
msgid "Error al eliminar la autenticacion para sitio externo"
msgstr ""

#: api_app/views/external_system.py:44
msgid "Se obtuvieron los sistemas externos de forma exitosa"
msgstr ""

#: api_app/views/external_system.py:55
msgid "Error al obtener los sistemas externos"
msgstr ""

#: api_app/views/external_system.py:72
msgid "Se creo el sistema externo de forma exitosa"
msgstr ""

#: api_app/views/external_system.py:86
msgid "Error al crear el sistema externo"
msgstr ""

#: api_app/views/external_system.py:103
msgid "Se actualizo el sistema externo de forma exitosa"
msgstr ""

#: api_app/views/external_system.py:120
msgid "No existe el sistema externo que se quiere actualizar"
msgstr ""

#: api_app/views/external_system.py:126
msgid "Error al actualizar el sistema externo"
msgstr ""

#: api_app/views/external_system.py:142
msgid "Se obtuvo la informacion del sistema externo de forma exitosa"
msgstr ""

#: api_app/views/external_system.py:151
msgid "No existe el sistema externo para obtener el detalle"
msgstr ""

#: api_app/views/external_system.py:157
msgid "Error al obtener el detalle del sistema externo"
msgstr ""

#: api_app/views/external_system.py:183
msgid "Error al obtener los agentes"
msgstr ""

#: api_app/views/form.py:43
msgid "Se obtuvieron los formularios de forma exitosa"
msgstr ""

#: api_app/views/form.py:53
msgid "Error al obtener los formularios"
msgstr ""

#: api_app/views/form.py:70
msgid "Se creo el formulario de forma exitosa"
msgstr ""

#: api_app/views/form.py:84
msgid "Error al crear el formulario"
msgstr ""

#: api_app/views/form.py:101
msgid "Se actualizo el formulario de forma exitosa"
msgstr ""

#: api_app/views/form.py:107
msgid "No está permitido actualizar un formulario con calificaciones"
msgstr ""

#: api_app/views/form.py:124
msgid "No existe el formulario que se quiere actualizar"
msgstr ""

#: api_app/views/form.py:130
msgid "Error al actualizar el formulario"
msgstr ""

#: api_app/views/form.py:146
msgid "Se obtuvo la informacion del formulario de forma exitosa"
msgstr ""

#: api_app/views/form.py:155
msgid "No existe el formulario para obtener el detalle"
msgstr ""

#: api_app/views/form.py:161
msgid "Error al obtener el detalle del formulario"
msgstr ""

#: api_app/views/form.py:177
msgid "Se elimino el formulario de forma exitosa"
msgstr ""

#: api_app/views/form.py:183
msgid "No está permitido eliminar un formulario con calificaciones"
msgstr ""

#: api_app/views/form.py:191
msgid "No existe el formulario"
msgstr ""

#: api_app/views/form.py:196
msgid "Error al eliminar el formulario"
msgstr ""

#: api_app/views/form.py:211
msgid "Se oculto el formulario de forma exitosa"
msgstr ""

#: api_app/views/form.py:219
msgid "Error al ocultar el formulario"
msgstr ""

#: api_app/views/form.py:234
msgid "Se desoculto el formulario de forma exitosa"
msgstr ""

#: api_app/views/form.py:242
msgid "Error al desocultar el formulario"
msgstr ""

#: api_app/views/grabaciones.py:94
msgid "Exportación de grabaciones Zip en proceso"
msgstr ""

#: api_app/views/group_of_hour.py:46
msgid "Se obtuvieron los grupos horarios de forma exitosa"
msgstr ""

#: api_app/views/group_of_hour.py:56
msgid "Error al obtener los grupos horarios"
msgstr ""

#: api_app/views/group_of_hour.py:73
msgid "Se creo el grupo horario de forma exitosa"
msgstr ""

#: api_app/views/group_of_hour.py:79
msgid ""
"Se creo el grupo horario pero no se pudo cargar la configuración telefónica"
msgstr ""

#: api_app/views/group_of_hour.py:91
msgid "Error al crear el grupo horario"
msgstr ""

#: api_app/views/group_of_hour.py:108
msgid "Se actualizo el grupo horario de forma exitosa"
msgstr ""

#: api_app/views/group_of_hour.py:117
msgid ""
"Se actualizo el grupo horario pero no se pudo cargar la configuración "
"telefónica"
msgstr ""

#: api_app/views/group_of_hour.py:134
msgid "Error al actualizar el grupo horario"
msgstr ""

#: api_app/views/group_of_hour.py:150
msgid "Se obtuvo la informacion el grupo horario de forma exitosa"
msgstr ""

#: api_app/views/group_of_hour.py:164
msgid "Error al obtener el detalle del grupo horario"
msgstr ""

#: api_app/views/group_of_hour.py:180
msgid "Se elimino el grupo horario de forma exitosa"
msgstr ""

#: api_app/views/group_of_hour.py:186
msgid ""
"No está permitido eliminar un grupo horario asociado a Validacion Fecha Hora"
msgstr ""

#: api_app/views/group_of_hour.py:191
msgid ""
"Se elimino el grupo horario pero no se pudo cargar la configuración "
"telefónica"
msgstr ""

#: api_app/views/inbound_route.py:46 api_app/views/outbound_route.py:49
msgid "Se obtuvieron las rutas entrantes de forma exitosa"
msgstr ""

#: api_app/views/inbound_route.py:56
msgid "Error al obtener las rutas entrantes"
msgstr ""

#: api_app/views/inbound_route.py:73
msgid "Se creo la ruta entrante de forma exitosa"
msgstr ""

#: api_app/views/inbound_route.py:79
msgid ""
"Se creo la ruta entrante pero no se pudo cargar la configuración telefónica"
msgstr ""

#: api_app/views/inbound_route.py:90
msgid "Error al crear la ruta entrante"
msgstr ""

#: api_app/views/inbound_route.py:107
msgid "Se actualizo la ruta entrante de forma exitosa"
msgstr ""

#: api_app/views/inbound_route.py:118 api_app/views/inbound_route.py:195
msgid ""
"Se actualizo la ruta entrante pero no se pudo cargar la configuración "
"telefónica"
msgstr ""

#: api_app/views/inbound_route.py:129
msgid "No existe la ruta entrante que se quiere actualizar"
msgstr ""

#: api_app/views/inbound_route.py:135
msgid "Error al actualizar la ruta entrante"
msgstr ""

#: api_app/views/inbound_route.py:151
msgid "Se obtuvo la informacion de la ruta entrante de forma exitosa"
msgstr ""

#: api_app/views/inbound_route.py:160
msgid "No existe la ruta entrante para obtener el detalle"
msgstr ""

#: api_app/views/inbound_route.py:166
msgid "Error al obtener el detalle de la ruta entrante"
msgstr ""

#: api_app/views/inbound_route.py:182
msgid "Se elimino la ruta entrante de forma exitosa"
msgstr ""

#: api_app/views/inbound_route.py:189
msgid ""
"No está permitido eliminar una Ruta Entrante asociada con una campaña que "
"tiene una Ruta Saliente."
msgstr ""

#: api_app/views/inbound_route.py:201
msgid "No existe la ruta entrante"
msgstr ""

#: api_app/views/inbound_route.py:206
msgid "Error al eliminar la ruta entrante"
msgstr ""

#: api_app/views/inbound_route.py:221
msgid "Se obtuvo la informacion de los destinos de forma exitosa"
msgstr ""

#: api_app/views/inbound_route.py:240
msgid "Error al obtener los destinos de las rutas entrantes"
msgstr ""

#: api_app/views/ivr.py:54
msgid "Se obtuvieron los IVRs de forma exitosa"
msgstr ""

#: api_app/views/ivr.py:60
msgid "Error al obtener los ivrs"
msgstr ""

#: api_app/views/ivr.py:82
msgid "Se creo el IVR de forma exitosa"
msgstr ""

#: api_app/views/ivr.py:84
msgid "Se creo el IVR pero no se pudo cargar la configuración telefónica"
msgstr ""

#: api_app/views/ivr.py:95 api_app/views/ivr.py:137 api_app/views/ivr.py:205
msgid "Error en los datos enviados"
msgstr ""

#: api_app/views/ivr.py:101
msgid "Error al crear el IVR"
msgstr ""

#: api_app/views/ivr.py:124
msgid "Se actualizo el IVR de forma exitosa"
msgstr ""

#: api_app/views/ivr.py:126
msgid "Se actualizo el IVR pero no se pudo cargar la configuración telefónica"
msgstr ""

#: api_app/views/ivr.py:143 api_app/views/ivr.py:171 api_app/views/ivr.py:222
msgid "No existe el IVR"
msgstr ""

#: api_app/views/ivr.py:148
msgid "Error al actualizar el IVR"
msgstr ""

#: api_app/views/ivr.py:165
msgid "Se obtuvo la informacion del IVR de forma exitosa"
msgstr ""

#: api_app/views/ivr.py:176
msgid "Error al obtener el detalle del IVR"
msgstr ""

#: api_app/views/ivr.py:194
msgid "No se puede eliminar un objeto que es destino en un flujo de llamada."
msgstr ""

#: api_app/views/ivr.py:199
#, python-brace-format
msgid ""
"No se puede eliminar la campaña.Es usada como destino failover de las "
"campañas: {0}"
msgstr ""

#: api_app/views/ivr.py:208
msgid "Se elimino el IVR de forma exitosa"
msgstr ""

#: api_app/views/ivr.py:210
msgid "Se elimino el IVR pero no se pudo cargar la configuración telefónica"
msgstr ""

#: api_app/views/ivr.py:227
msgid "Error al eliminar el IVR"
msgstr ""

#: api_app/views/ivr.py:246
msgid "Se obtuvieron los archivos de audio de forma exitosa"
msgstr ""

#: api_app/views/ivr.py:252
msgid "Error al obtener los archivos de audio"
msgstr ""

#: api_app/views/ivr.py:280
msgid "Se obtuvieron los destinos de forma exitosa"
msgstr ""

#: api_app/views/ivr.py:286
msgid "Error al obtener los destinos"
msgstr ""

#: api_app/views/outbound_route.py:59
msgid "Error al obtener las rutas salientes"
msgstr ""

#: api_app/views/outbound_route.py:76
msgid "Se creo la ruta saliente de forma exitosa"
msgstr ""

#: api_app/views/outbound_route.py:82
msgid ""
"Se creo la ruta saliente pero no se pudo cargar la configuración telefónica"
msgstr ""

#: api_app/views/outbound_route.py:93
msgid "Error al crear la ruta saliente"
msgstr ""

#: api_app/views/outbound_route.py:110
msgid "Se actualizo la ruta saliente de forma exitosa"
msgstr ""

#: api_app/views/outbound_route.py:119
msgid ""
"Se actualizo la ruta saliente pero no se pudo cargar la configuración "
"telefónica"
msgstr ""

#: api_app/views/outbound_route.py:130
msgid "No existe la ruta saliente que se quiere actualizar"
msgstr ""

#: api_app/views/outbound_route.py:136
msgid "Error al actualizar la ruta saliente"
msgstr ""

#: api_app/views/outbound_route.py:152
msgid "Se obtuvo la informacion de la ruta saliente de forma exitosa"
msgstr ""

#: api_app/views/outbound_route.py:162
msgid "No existe la ruta saliente para obtener el detalle"
msgstr ""

#: api_app/views/outbound_route.py:168
msgid "Error al obtener el detalle de la ruta saliente"
msgstr ""

#: api_app/views/outbound_route.py:184
msgid "Se elimino la ruta saliente de forma exitosa"
msgstr ""

#: api_app/views/outbound_route.py:190
msgid "No está permitido eliminar una ruta saliente asociada a una campaña"
msgstr ""

#: api_app/views/outbound_route.py:195
msgid "Se elimino la ruta pero no se pudo cargar la configuración telefónica"
msgstr ""

#: api_app/views/outbound_route.py:201 api_app/views/outbound_route.py:261
msgid "No existe la ruta saliente"
msgstr ""

#: api_app/views/outbound_route.py:206
msgid "Error al eliminar la ruta saliente"
msgstr ""

#: api_app/views/outbound_route.py:221
msgid "Se obtuvieron las troncales sip de forma exitosa"
msgstr ""

#: api_app/views/outbound_route.py:231
msgid "Error al obtener las troncales"
msgstr ""

#: api_app/views/outbound_route.py:246
msgid ""
"Se obtuvieron las troncales huerfanas de la ruta saliente de forma exitosa"
msgstr ""

#: api_app/views/outbound_route.py:266
msgid "Error al obtener las troncales huerfanas de la ruta saliente"
msgstr ""

#: api_app/views/outbound_route.py:281
msgid "El orden de las rutas salientes se actualizo de forma exitosa"
msgstr ""

#: api_app/views/outbound_route.py:287
msgid "El orden es un campo requerido"
msgstr ""

#: api_app/views/outbound_route.py:292 api_app/views/outbound_route.py:297
msgid "El orden debe ser una lista de numeros enteros"
msgstr ""

#: api_app/views/outbound_route.py:302
msgid "El nuevo orden no coincide con la cantidad de rutas saliente"
msgstr ""

#: api_app/views/outbound_route.py:309
msgid "No puede haber orden repetido en las rutas salientes"
msgstr ""

#: api_app/views/outbound_route.py:331
msgid "Error al actualizar el orden de las rutas salientes"
msgstr ""

#: api_app/views/pause.py:66 api_app/views/pause_set.py:44
msgid "Se obtuvieron las pausas de forma exitosa"
msgstr ""

#: api_app/views/pause.py:76 api_app/views/pause_set.py:54
msgid "Error al obtener las pausas"
msgstr ""

#: api_app/views/pause.py:92
msgid "Se creo la pausa de forma exitosa"
msgstr ""

#: api_app/views/pause.py:106 api_app/views/pause.py:157
#: api_app/views/pause.py:214 api_app/views/pause.py:250
msgid "Error al sincronizar la pausa con asterisk"
msgstr ""

#: api_app/views/pause.py:123
msgid "Error al crear la pausa"
msgstr ""

#: api_app/views/pause.py:140
msgid "Se actualizo la pausa de forma exitosa"
msgstr ""

#: api_app/views/pause.py:181
msgid "Error al actualizar la pausa"
msgstr ""

#: api_app/views/pause.py:197
msgid "Se elimino la pausa de forma exitosa"
msgstr ""

#: api_app/views/pause.py:203
msgid "No está permitido eliminar un pausa con configuraciones creadas"
msgstr ""

#: api_app/views/pause.py:225
msgid "Error al eliminar la pausa"
msgstr ""

#: api_app/views/pause.py:240
msgid "Se reactivo la pausa de forma exitosa"
msgstr ""

#: api_app/views/pause.py:261
msgid "Error al reactivar la pausa"
msgstr ""

#: api_app/views/pause.py:276
msgid "Se obtuvo la informacion de la pausa de forma exitosa"
msgstr ""

#: api_app/views/pause.py:291
msgid "Error al obtener el detalle de la pausa"
msgstr ""

#: api_app/views/pause_set.py:69
msgid "Se obtuvieron los conjuntos de pausas de forma exitosa"
msgstr ""

#: api_app/views/pause_set.py:80
msgid "Error al obtener los conjuntos de pausas"
msgstr ""

#: api_app/views/pause_set.py:95
msgid "Se obtuvo la informacion del conjunto de pausa de forma exitosa"
msgstr ""

#: api_app/views/pause_set.py:112
msgid "Error al obtener el detalle del conjunto de pausa"
msgstr ""

#: api_app/views/pause_set.py:128
msgid "Se creo el conjunto de pausa de forma exitosa"
msgstr ""

#: api_app/views/pause_set.py:133 api_app/views/pause_set.py:169
msgid "El nombre del conjunto es requerido"
msgstr ""

#: api_app/views/pause_set.py:137
msgid "Debe existir al menos una pausa en el conjunto"
msgstr ""

#: api_app/views/pause_set.py:150
msgid "Error al crear el conjunto de pausa"
msgstr ""

#: api_app/views/pause_set.py:165
msgid "Se actualizo el conjunto de pausa de forma exitosa"
msgstr ""

#: api_app/views/pause_set.py:178 api_app/views/pause_set.py:212
#: api_app/views/pause_set.py:254
msgid "No existe el conjunto de pausa"
msgstr ""

#: api_app/views/pause_set.py:183
msgid "Error al actualizar el conjunto de pausa"
msgstr ""

#: api_app/views/pause_set.py:198
msgid "Se elimino el conjunto de pausa de forma exitosa"
msgstr ""

#: api_app/views/pause_set.py:205
msgid ""
"No puedes borrar un conjunto de pausas que esta asignado a un grupo de "
"agentes"
msgstr ""

#: api_app/views/pause_set.py:217
msgid "Error al eliminar el conjunto de pausa"
msgstr ""

#: api_app/views/pause_set.py:232
msgid "Se creo la configuracion de pausa de forma exitosa"
msgstr ""

#: api_app/views/pause_set.py:239
msgid "El timeout debe ser mayor a cero"
msgstr ""

#: api_app/views/pause_set.py:246
msgid "No existe la pausa"
msgstr ""

#: api_app/views/pause_set.py:264
msgid "Error al crear la configuracion de pausa"
msgstr ""

#: api_app/views/pause_set.py:279
msgid "Se elimino la configuracion de pausa de forma exitosa"
msgstr ""

#: api_app/views/pause_set.py:287
msgid "No puedes dejar a un Conjunto de Pausas vacio"
msgstr ""

#: api_app/views/pause_set.py:292 api_app/views/pause_set.py:322
msgid "No existe la configuracion de pausa"
msgstr ""

#: api_app/views/pause_set.py:297
msgid "Error al eliminar la configuracion de pausa"
msgstr ""

#: api_app/views/pause_set.py:312
msgid "Se actualizo la configuracion de pausa de forma exitosa"
msgstr ""

#: api_app/views/pause_set.py:327
msgid "Error al actualizar la configuracion de pausa"
msgstr ""

#: api_app/views/supervisor.py:108 api_app/views/supervisor.py:112
#: api_app/views/supervisor.py:115
msgid "Filtro \"type\" inválido"
msgstr ""

#: api_app/views/supervisor.py:124
msgid "Filtro \"agent\" inválido"
msgstr ""

#: api_app/views/supervisor.py:128
msgid "Agente inexistente"
msgstr ""

#: api_app/views/supervisor.py:139 api_app/views/supervisor.py:144
#: api_app/views/supervisor.py:147
msgid "Filtro \"status\" inválido"
msgstr ""

#: api_app/views/supervisor.py:300 api_app/views/supervisor.py:346
msgid "ID Agenda incorrecto"
msgstr ""

#: api_app/views/supervisor.py:307
msgid "ID Agente incorrecto"
msgstr ""

#: api_app/views/supervisor.py:315 api_app/views/supervisor.py:353
msgid "No tiene permiso para editar esta Agenda"
msgstr ""

#: api_app/views/supervisor.py:372
msgid "Recibidas"
msgstr ""

#: api_app/views/supervisor.py:373
msgid "Efectuadas"
msgstr ""

#: api_app/views/supervisor.py:374
msgid "Atendidas"
msgstr ""

#: api_app/views/supervisor.py:375
msgid "Conectadas"
msgstr ""

#: api_app/views/supervisor.py:376
msgid "No Conectadas"
msgstr ""

#: api_app/views/supervisor.py:377
msgid "Abandonadas"
msgstr ""

#: api_app/views/supervisor.py:378
msgid "Expiradas"
msgstr ""

#: api_app/views/supervisor.py:379
msgid "Tiempo de Espera de Conexión(prom.)"
msgstr ""

#: api_app/views/supervisor.py:380
msgid "Tiempo de Espera de Atención(prom.)"
msgstr ""

#: api_app/views/supervisor.py:381
msgid "Tiempo de Abandono(prom.)"
msgstr ""

#: api_app/views/supervisor.py:384
msgid "Efectuadas Manuales"
msgstr ""

#: api_app/views/supervisor.py:385
msgid "Conectadas Manuales"
msgstr ""

#: api_app/views/supervisor.py:386
msgid "No Conectadas Manuales"
msgstr ""

#: api_app/views/supervisor.py:387
msgid "Tiempo de Espera de Conexión Manuales(prom.)"
msgstr ""

#: api_app/views/supervisor.py:513
msgid "Exportación de CSV en proceso"
msgstr ""

#: api_app/views/supervisor.py:561
msgid "Exportación de contactados a .csv en proceso"
msgstr ""

#: api_app/views/supervisor.py:608
msgid "Exportación de calificados a .csv en proceso"
msgstr ""

#: api_app/views/supervisor.py:655
msgid "Exportación de no atendidos a .csv en proceso"
msgstr ""

#: api_app/views/supervisor.py:667
msgid "Finalizado"
msgstr ""

#: api_app/views/supervisor.py:669
msgid "Liberado"
msgstr ""

#: api_app/views/supervisor.py:671
msgid "Reservado"
msgstr ""

#: api_app/views/supervisor.py:755
msgid "Exportación de calificaciones a .csv en proceso"
msgstr ""

#: api_app/views/supervisor.py:800
msgid "Exportación de Formularios de Gestiones a .csv en proceso"
msgstr ""

#: api_app/views/wombat_dialer.py:47 api_app/views/wombat_dialer.py:91
msgid "Refrescando. Espere al menos 15 segundos mientras finaliza el proceso."
msgstr ""

#: notification_app/message/apps.py:46
msgid "Lista de los mensajes de correos que el sistema soporta"
msgstr ""

#: notification_app/message/apps.py:50
msgid "Obtiene detalle de un mensaje de correo"
msgstr ""
